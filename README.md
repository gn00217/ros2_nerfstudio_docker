# Docker Image For NeRFStudio and ROS2

This is the installation steps required to build a docker image for the purposes of NeRF simulation of environments. 

Be aware this image is 38.7GB.

## Installation

```bash
cd ros2_nerfstudio_docker
```
```bash
docker build . -t ros-nerf:1.0
```

## Usage

```bash
docker run -it --gpus all -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 --user root -v /tmp/.X11-unix:/tmp/.X11-unix ros-nerf/version1.0:latest bash
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
